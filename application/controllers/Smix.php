<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Smix extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}

	public function services() {

		if (!file_exists(APPPATH . '/views/service/' . '.php')) {
			// Whoops, we don't have a page for that!
			show_404();
		}
		echo "test";
		$this->load->view('header');
		$this->load->view('service');
		$this->load->view('footer');
	}

	public function portfolio() {

		$this->load->view('header');
		$this->load->view('portfolio');
		$this->load->view('footer');
	}

	public function about() {

		$this->load->view('header');
		$this->load->view('about');
		$this->load->view('footer');
	}

	public function contact() {

		$this->load->view('header');
		$this->load->view('contact');
		$this->load->view('footer');
	}

	public function email_test() {

		$this->email_to_adminstaff($_POST);
		//  echo $this->email->print_debugger();
	}

	private function email_to_adminstaff($data = array()) {
		//    $this->load->view('admin/view_plot', $__data);
		$mesg = $this->load->view('edm/admin_email', $data, true);
		$this->load->library('email');
		$config = array(
			'charset' => 'utf-8',
			'wordwrap' => TRUE,
			'mailtype' => 'text'
		);

		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from('info@bidartwork.com', 'Info');
		//     $this->email->from('info@insigniabiz.com', 'Insignia Business Solutions');
		$this->email->to('arman.anwar@gmail.com , info@insigniabiz.com');
		//    $this->email->cc('Joanne.Lim@virginactive.com.sg, Charlene.Teo@virginactive.com.sg');
		//    $this->email->bcc('ridz@octasialabs.com');
		$this->email->subject('New query at IBS');
		//  $this->email->cc('ridz@octasialabs.com ,iffy431@bidartwork.com');

		$this->email->message($mesg);
		$mail = $this->email->send();
	}

}
