<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Autoload the required files
require_once 'autoload.php';

use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;

//use Facebook\FacebookJavaScriptLoginHelper;

class Facebook {

    var $ci;
    var $helper;
    var $session;

    public function __construct() {
        $this->ci = & get_instance();

        // Initialize the SDK
        FacebookSession::setDefaultApplication($this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook_sec'));
    }

    private function load_session() {
        $this->ci = & get_instance();

        if ($this->ci->session->userdata('access_token')) {

            $this->session = new FacebookSession($this->ci->session->userdata('access_token'));

            // Validate the access_token to make sure it's still valid
            try {
                // var_dump($this->session);
                if (!$this->session->validate()) {
                    $this->session = null;
                }

                //   var_dump($this->session);
            } catch (Exception $e) {
                // Catch any exceptions
                $this->session = null;
            }
        }
    }

    private function load_server_session() {
        $this->ci = & get_instance();

        if ($this->ci->session->userdata('access_token')) {

            $this->session = new FacebookSession($this->ci->config->item('api_id', 'facebook') .'|'. $this->ci->config->item('app_secret', 'facebook_sec'));

            // Validate the access_token to make sure it's still valid
            try {
                // var_dump($this->session);
                if (!$this->session->validate()) {
                    $this->session = null;
                }

                //   var_dump($this->session);
            } catch (Exception $e) {
                // Catch any exceptions
                $this->session = null;
            }
        }
    }
    
    
    public static function isLoggedIn($access_token = null) {
        $id = 0;
        if (isset($access_token)) {
            try {
                $session = new FacebookSession($access_token);

                $request = new FacebookRequest($session, 'GET', '/me');
                $response = $request->execute();

                $graphObject = $response->getGraphObject();

                $id = $graphObject->getProperty("id");
            } catch (Exception $ex) {
                echo $ex;
            }
        }

        return $id != 0;
    }

    /**
     * Returns the current user's info as an array.
     */
    public function get_user() {
        $this->load_session();
        if ($this->session) {
            /**
             * Retrieve User’s Profile Information
             */
            // Graph API to request user data
            $request = new FacebookRequest($this->session, 'GET', '/me');
            $response = $request->execute();
            // Get response as an array
            $user = $response->getGraphObject()->asArray();

            return $user;
        }
        return false;
    }

    public function get_taggable_friends() {
        $this->load_session();
        if ($this->session) {
            /**
             * Retrieve User’s Profile Information
             */
            // Graph API to request user data
            $request = new FacebookRequest($this->session, 'GET', '/me/taggable_friends');
            $response = $request->execute();

            // Get response as an array
            return $response->getGraphObject()->asArray();
        }
        return false;
    }

    public function get_invitable_friends() {
        $this->load_session();
        if ($this->session) {
            /**
             * Retrieve User’s Profile Information
             */
            // Graph API to request user data
            $request = new FacebookRequest($this->session, 'GET', '/me/invitable_friends');
            $response = $request->execute();

            // Get response as an array
            return $response->getGraphObject()->asArray();
        }
        return false;
    }

    public function upload_image($image_name,$image_description) {
        $this->load_session();
        if ($this->session) {

            $attachment = array(
                'access_token' => $this->ci->session->userdata('access_token'),
                'message' => 'message',
                'name' => "I don't do drugs because...",
                'description' => "$image_description",
                'picture' => base_url().'/assets/gallery/watermark/watermark_2/'.$image_name,
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/me/feed');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $attachment);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output 
            $result = curl_exec($ch);
            curl_close($ch);

        }
    }

    public function get_login_url($path = '') {
        $this->ci = & get_instance();

        $helper = new FacebookRedirectLoginHelper($this->ci->config->item('redirect_url', 'facebook') . $path);
        $loginUrl = $helper->getLoginUrl($this->ci->config->item('permissions', 'facebook_sec'));
        return $loginUrl;
    }

    public function get_playing_friends($id) {
        $this->load_server_session();
        if ($this->session) {
            echo 'in session';
            /**
             * Retrieve User’s Profile Information
             */
            // Graph API to request user data
            try{
            $request = new FacebookRequest($this->session, 'GET', '/'.$id.'/friends?fields=installed');
            $response = $request->execute();

            // Get response as an array
            return $response->getGraphObject()->asArray();
            }catch(FacebookAuthorizationException $ex){
                //echo $ex . '<br/>';
            
            }
        }
        return false;
    }

    
    
}
