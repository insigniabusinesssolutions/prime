<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['facebook_app_id'] = '789816211134974';
$config['facebook_api_secret'] = '030a1c14b14ad6282c651db627343ffb';
$config['facebook_default_scope'] = 'user_friends';

$config['name'] = "Fly With Hello Kitty";

$config['caption'] = 'Fly With Hello Kitty caption';

$config['locale'] = 'en_us';
$config['title'] = "Play Fly With Hello Kitty Game and win exclusive prizes!";

$config['image'] = 'assets/img/fb-display.jpg';
$config['description'] = 'Want to experience The EVA Air Hello Kitty Jet? Play our Fly With Hello Kitty Game and stand a chance to win trips to Houston and Taipei on the Hello Kitty Jet PLUS win exclusive Hello Kitty Merchandise!';
$config['canvas_url'] = 'https://apps.facebook.com/hellokitty_test/';

$config['share_link'] = 'https://localhost/octasialabs/eva-air-fly-with-kitty/';
$config['place_id'] = '105565836144069';

$config['twitter_message'] = 'Play Fly With Hello Kitty Game and stand a chance to win trips to Houston and Taipei on the Hello Kitty Jet PLUS win exclusive Hello Kitty Merchandise!';

$config['facebook']['share_link'] = 'https://localhost/octasialabs/eva-air-fly-with-kitty/';

$config['facebook']['method'] = 'feed';
$config['facebook']['api_id'] = '789816211134974';
$config['facebook_sec']['app_secret'] = '030a1c14b14ad6282c651db627343ffb';
$config['facebook']['redirect_url'] = 'https://localhost/octasialabs/eva-air-fly-with-kitty/';

$config['facebook']['name'] = "Hello Kitty";
$config['facebook']['caption'] = "Show us how you embrace harmony and fun at work with your colleagues of different cultural backgrounds!";
$config['facebook']['link'] = "https://apps.facebook.com/hellokitty_test/";
$config['facebook']['description'] = "Want to experience The EVA Air Hello Kitty Jet? Play our Fly With Hello Kitty Game and stand a chance to win trips to Houston and Taipei on the Hello Kitty Jet PLUS win exclusive Hello Kitty Merchandise!";
$config['facebook']['picture'] = "assets/img/fb-display.jpg";


$config['facebook_sec']['permissions'] = array(
    'user_friends'
);

