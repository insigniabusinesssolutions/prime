<div class="background">     
    <div class="row">
        <h1><img style="width: 345px;" src = "<?php echo base_url('assets/img/logo.png'); ?>"></h1>
    </div>
    <div class="row">
        <div class="june-21">
            <h1 class="title">Fly With Hello kitty</h1>
        </div>
    </div>
    <div class="row">
        <h1><img src = "<?php echo base_url('assets/img/singapore.png'); ?>"></h1>
    </div>
    <div class="small-10 small-centered column">
        <div class="row">
            <p class="text-center stant-to-win zero-margin">Play, collect all 5 stamps and stand a chance to win tickets to Taipei or Houston on the new EVA Air Hello Kitty Shining Star Jet 
            </p>
            <br/>
            <p class="text-center">Campaign period: <span class="text-bold">22 July - 2 Sep 2015</span></p>
        </div>
    </div>
    <br/>
    <br/>
    <br/>
    <div class="small-10 small-centered column">
        <div class="row">
            <div class="how-to">
                <h5 class="font-cooper text-blue">Welcome Aboard Hello Kitty Shining Star!</h5>
                <p class="text-black">Are you ready for our Hello Kitty Shining Star adventure? Join us, as we take you on a journey to experience the new Hello Kitty Shining Star jet!
                </p>
				<h6 class="font-cooper text-blue left marg-left">How to Play:</h6>
				<div class="clearfix"></div>
                <p class="step text-left"><span class="text-bold">1) </span>There are 5 games in total to complete your Journey.</p>
                <p class="step text-left"><span class="text-bold">2) </span> 1 game will be released every week starting from 29 June 2015.</p>
                <p class="step text-left"><span class="text-bold">3) </span>Completion of every game gets you 1 stamp. </p>
                <p class="step text-left"><span class="text-bold">4) </span>Collect all 5 stamps and you are automatically in the draw to win the grand prize of a pair of tickets on our Shining Star Jet to Houston!</p>
                <ul class="g-prize">
                    <li><span class="text-blue text-bold">Grand Prize:</span> 1x Elite Class ticket to Houston</li>
                    <li><span class="text-blue text-bold">Second Prize: </span>1x  Elite class Ticket to Taipei</li>
                </ul>
                <p class="step text-left"><span class="text-bold">5) </span>Earn tokens along the way and at the end of the 5 games, we will open our inflight shopping store for you to bid for items with your tokens. The highest bidder will win these exclusive gifts from us!</p>
                <br/>
                <p class="stext-center text-blue text-bold">Have a great adventure onboard Shining Star!</p>
            </div>
        </div>
    </div>

    <div class="row not-logged-in">
        <a href="<?php echo site_url(''); ?>" class="fb-login"><img src="<?php echo base_url('assets/img/fb-login.png'); ?>" /></a>
    </div>
    <br/>
    <div class="row">
        <div class="term-cond">
            <a class="text-underline text-white" href="https://www.facebook.com/evaairwayscorp.sg?sk=notes_drafts" target="_blank">Terms & Conditions apply.</a>
        </div>
    </div>
</div>
<br/>
<br/><br/>

<script type="text/javascript">
    $(document).ready(function () {
        init_facebook(function (response) {


        });

        $("a.fb-login").click(function (e) {
            e.preventDefault();
            var $this = this;

            if (is_mobile())
            {
                var permissionUrl = "<?php echo $login_url; ?>";
                FB.getLoginStatus(function (response) {
                    if (response.authResponse) {
                        $.post("<?php echo base_url('/hellokitty/initialize') ?>", {data: response.authResponse}, function (msg) {
                            window.location = $($this).attr('href');
                        });
                    }
                    else {
                        window.location = permissionUrl;
                    }
                });
            } else {

                FB.login(function (response) {
                    if (response.authResponse) {
                        $.post("<?php echo base_url('/hellokitty/initialize') ?>", {data: response.authResponse}, function (msg) {
                            window.location = $($this).attr('href');
                        });
                    } else {
                        console.log(response);
                    }
                }, {scope: 'user_friends', return_scopes: true//, auth_type: 'rerequest'
                });

            }
        });

    });
</script>
