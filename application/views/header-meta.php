
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta name="viewport" content="width=640, maximum-scale=1">
<?php if (isset($custom) ) { ?>
    <meta property="og:title" content="<?php echo $photo['image_description']; ?>" />
    <meta property="og:url" content="<?php echo $this->config->item('canvas_url') . 'harmony/entry/' . $photo['id']; ?>" />
    <meta property="og:image" content="<?php echo base_url('assets/gallery/' . $photo['image_name']); ?>" /> 
    <meta property="og:description" content="<?php echo $photo['image_description']; ?>" />
<?php } else { ?>
    <meta property="og:title" content="<?php echo $this->config->item('title'); ?>" />
    <meta property="og:url" content="<?php echo $this->config->item('canvas_url'); ?>" />
    <meta property="og:image" content="<?php echo base_url($this->config->item('image')); ?>" />
    <meta property="og:description" content="<?php echo $this->config->item('description'); ?>" />
<?php } ?>
<meta property="og:type" content="website" />
<meta property="fb:app_id" content="<?php echo $this->config->item('facebook_app_id'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Hello kitty</title>
