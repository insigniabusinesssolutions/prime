
<!DOCTYPE html>
<html class="no-js">
    
<!-- Mirrored from technext.github.io/timer-html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Mar 2016 09:18:52 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.html">
        <title>Smix IT</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/ionicons.min.css'); ?>">
        <!-- animate css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css'); ?>">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/slider.css'); ?>">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.fancybox.css'); ?>">
        <!-- template main css file -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css'); ?>">
        <!-- responsive css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css'); ?>">
        
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="<?php echo base_url('assets/js/vendor/modernizr-2.6.2.min.js'); ?>"></script>
        <!-- jquery -->
       <script src="//code.jquery.com/jquery-1.12.0.min.js"></script> 
        <!-- owl carouserl js -->
        <script src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>
        <!-- bootstrap js -->

        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <!-- wow js -->
        <script src="<?php echo base_url('assets/js/wow.min.js'); ?>"></script>
        <!-- slider js -->
        <script src="<?php echo base_url('assets/js/slider.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.fancybox.js'); ?>"></script>
        <!-- template main js -->
        <script src="<?php echo base_url('assets/js/main.js'); ?>"></script>
    </head>
    <body>
        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->
                    
                    <!-- logo -->
                    <div class="navbar-brand">
                        <a href="<?php echo base_url(); ?>" >
                            <h1 style="margin:0px;font-style: oblique;color: #02BDD5;font-weight: 700;">Prime Business Solutions</h1>
                            <!-- <img src="images/logo.png" alt=""> -->
                        </a>
                    </div>
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="<?php echo base_url(); ?>" >Home</a>
                            </li>
                            <li><a href="<?php echo base_url("Smix/services"); ?>">Services</a></li>
                            <li><a href="<?php echo base_url("Smix/about"); ?>">About</a></li>
                            <li><a href="<?php echo base_url("Smix/portfolio"); ?>">Portfolio</a></li>
                            <!-- <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
                                <div class="dropdown-menu">zzzzzzzzzzzzzzzzzzzs
                                    <ul>
                                        <li><a href="404.html">404 Page</a></li>
                                        <li><a href="gallery.html">Gallery</a></li>
                                    </ul>
                                </div> -->
                            </li>
                            <!-- <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog <span class="caret"></span></a>
                                <div class="dropdown-menu">
                                    <ul>
                                        <li><a href="blog-fullwidth.html">Blog Full</a></li>
                                        <li><a href="blog-left-sidebar.html">Blog Left sidebar</a></li>
                                        <li><a href="blog-right-sidebar.html">Blog Right sidebar</a></li>
                                    </ul>
                                </div>
                            </li> -->
                            <li><a href="<?php echo base_url("Smix/contact"); ?>">Contact</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
        </header>