<link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/normalize.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/font.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/luggage.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/overley.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/slick-theme.css'); ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/slick.css'); ?>" />
<script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.custom.17475.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/foundation.min.js'); ?>"></script>

<script type="text/javascript">
      var base_url = "<?php echo base_url(); ?>";

    // alert(is_mobile());

    var $site = {
        base_url: "<?php echo base_url(); ?>",
        app_id: "<?php echo $this->config->item('facebook_app_id'); ?>",
    };

    function is_mobile()
    {
        var uagent = navigator.userAgent.toLowerCase();
        if (uagent.search("mobile") > -1)
            return true;
        else
            return false;
    }

    function inIframe() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }

    function open_popup(element) {
        if (inIframe() == true) {
            FB.Canvas.getPageInfo(
                    function(info) {
                        var scrollposition = info.scrollTop;
                        $(element).foundation('reveal', 'open');
                        $(element).css('margin-top', scrollposition + 'px');
                    }
            );
        }
        else {
            $(element).foundation('reveal', 'open');
        }
    }

    function init_facebook(callback_func) {
        window.fbAsyncInit = function() {
            FB.init({appId: '<?php echo $this->config->item('facebook_app_id'); ?>', status: true, xfbml: true, version: 'v2.3'});
            FB.getLoginStatus(function(response) {
                if (response.authResponse) {
                    console.log("Logged in");

                    //     check_facebook_permissions(permissions, fb_func); // check permissions if logged in


                } else {
                    console.log("Not logged in");

                    //window.location = "<?php echo base_url('/adac/login') ?>";

                }
                callback_func(response);
            });
            FB.Canvas.setAutoGrow();
            //fb_func();
        };

    }

    function check_facebook_permissions(permissions, callback_func) {
        FB.api("/me/permissions", function(response) {
            if (response && !response.error) {
                //   var perm_ok = false;
                $.each(response.data, function(index, perm) {
                    //console.log(perm);
                    if (typeof permissions !== 'undefined' && permissions.length > 0) {
                        if (permissions.indexOf(perm.permission) > -1)
                            if (perm.status == 'granted') {
                                permissions.splice(permissions.indexOf(perm.permission), 1);
                            }
                    }
                });
                if (typeof permissions !== 'undefined' && permissions.length > 0) {
                    console.log(permissions);
                    alert("You must give us appropriate permissions '" + permissions.join() + "' to fulfill this request");
                    FB.login(function(response) {
                        if (response.authResponse) {
                            console.log(response.authResponse);
                            callback_func(response);
                        } else {
                            console.log(response);
                        }
                    }, {scope: permissions.join(), return_scopes: true, auth_type: 'rerequest'
                    });
                } else {
                    callback_func(response);
                }
                /* handle the result */
            } else {
                console.log("error = " + response.error.message);
            }
        });
    }

    function scroll_fb_canvas(element) {
        FB.Canvas.getPageInfo(function(pageInfo) {

            // The scroll position of your app's iFrame.
            var iFrameScrollY = pageInfo.scrollTop;

            // The y position of the div you want to scroll up to.
            var targetDivY = $(element).position().top;
            //alert(targetDivY);

            // Only scroll if the user has scrolled the window beneath the target y position.
            if (iFrameScrollY > targetDivY) {
                var animOptions = {
                    // This function will be invoked each 'tick' of the animation.
                    step: function() {
                        // As we don't have control over the Facebook iFrame we have to ask the Facebook JS API to 
                        // perform the scroll for us.
                        FB.Canvas.scrollTo(0, this.y);
                    },
                    // How long you want the animation to last in ms.
                    duration: 200
                };
                // Here we are going to animate the 'y' property of the object from the 'iFrameScrollY' (the current 
                // scroll position) to the y position of your target div.
                $({y: iFrameScrollY}).animate({y: targetDivY}, animOptions);
            }
        });
    }

    $(document).ready(function() {
        (function(d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement('script');
            js.id = id;
            js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));
        $('div.wrapper').append('<div class="loaderwrap" id="cover"  style="display: none; "><img src="<?php echo base_url('assets/img/loader.gif'); ?>" id="loadera"/ ></div>');
    });


    function show_loader() {

        $('#cover').show();
    }
    function hide_loader() {
        $('#cover').hide();
    }

</script>