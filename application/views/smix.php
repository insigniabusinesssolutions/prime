<!doctype html>
<html class="no-js" lang="en">
    <head>
        <!-- META CHARS -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- PAGE TITLE -->
        <title>Insignia Business Solutions</title>

        <!-- STYLESHEETS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/smoothness/jquery-ui.css'); ?>" />
        <!--<link rel="stylesheet" href="<?php echo base_url('assets/css/login.css'); ?>" />--> 
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/app.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome-4.4.0/css/font-awesome.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css'); ?>" />


        <!-- JAVASCRIPTS -->
        <script src="<?php echo base_url('assets/js/vendor/modernizr.js'); ?>"></script>

    </head>
    <body>

        <!-- HEADER SECTION -->
        <div class="fixed header-section">

            <!-- TOPBAR SECTION -->
            <nav class="top-bar important-class" data-topbar role="navigation" data-options="is_hover: false">

                <!-- TITLE AREA & LOGO -->
                <ul class="title-area">
                    <li class="name">
                        <a href="#home"><img src="<?php echo base_url('assets/img/tandem_03.png'); ?>" alt="" id="logo-image"></a>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                </ul> <!-- END TITLE AREA & LOGO -->

                <!-- MENU ITEMS -->         
                <section class="top-bar-section">
                    <ul class="right">                      
                        <li><a href="#home">Home</a></li>
                        <li><a href="#Services">Services</a></li>
                        <li><a href="#portfolio">Portfolio</a></li>
                        <li><a href="#About">About us</a></li>
                        <li><a href="#Contact">Contact us</a></li>
                        <li class="marg-left"><a href="#" data-reveal-id="login-Modal">Login</a></li>
                    </ul>
                </section> <!-- END MENU ITEMS -->
            </nav> <!-- END TOPBAR SECTION -->
        </div> <!-- END HEADER SECTION -->

        <!-- Login Model -->

        <div id="login-Modal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
            <label for="email">Email Address</label>
            <input type="text" name="email" id="email">
            <!--<p class="error"></p>-->
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
            <p class="error"></p>
            <input type="submit" id="login" value="Log in">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>

        <!-- CONTENT FILL WHEN SCROLL = 0 -->
        <div class="header-fill"></div>
        <!-- CONTENT SECTION -->
        <div class="row section-one" id="home">
            <div class="large-8 columns large-offset-1">
                <div class="animated bounceInLeft">
                    <h3 class="welcome">Welcome to</h3>
                    <h1 class="title">INSIGNIA</h1>
                    <h3 class="subtitle">Business<span class="business"> Solutions</span></h3>
                    <a href="#" class="button btn-back"><span>Read More</span><span><img src="<?php echo base_url('assets/img/arrow_03.png'); ?>" /></span></a>
                </div>
            </div>
        </div>
        <!-- END CONTENT SECTION 1 -->
        <div class="leads">
            <img class="lead-img" src="<?php echo base_url('assets/img/banner.jpg') ?>"
        </div>
        <div class="section-two" id="Services">
            <div class="row">
                <div class="large-12 columns">
                    <div class="services">
                        <span>Our Services</span>
                    </div>   
                </div>
            </div>
            <div class="row">
                <div class="small-12 large-6 columns">
                    <div class="wow fadeInUp">
                        <div class="feature-1">
                            <div class="feature-1-img-div">
                                <img class="feature-1-img" src="<?php echo base_url('assets/img/1438912967_vector_66_11.png') ?>" />
                            </div>
                            <div class="feature-1-text">
                                <div class="feature-1-title">
                                    <span>Website Development</span>
                                </div>
                                <p class="feature-1-text-line">Developing for the web means so much more than a mere website. Constantly emerging technologies have made web development faster, more secure and more salable than ever before...</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="small-12 large-6 columns">
                    <div class="wow fadeInUp">
                        <div class="feature-2">
                            <div class="feature-2-img-div">
                                <img class="feature-2-img" src="<?php echo base_url('assets/img/1438912044_shopping_cart_loaded.png') ?>" />
                            </div>
                            <div class="feature-2-text">
                                <div class="feature-2-title">
                                    <span>E-Commerce Solutions</span>
                                </div>
                                <p class="feature-2-text-line">Most businesses today require e-commerce specialists to manage applications which conduct financial transactions over the Web. At Insignia, we are serious experts in creating e-commerce sites ...</p>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="small-12 large-6 columns">
                    <div class="wow fadeInUp">
                        <div class="feature-3">
                            <div class="feature-3-img-div">
                                <img class="feature-4-img" src="<?php echo base_url('assets/img/1438912760_web_basic_yellow.png') ?>" />
                            </div>
                            <div class="feature-3-text">
                                <div class="feature-3-title">
                                    <span>CRM Solutions</span>
                                </div>
                                <p class="feature-3-text-line">Our wide expertise in the CRM development and design niche has garnered us respected clients in various sectors, industries and areas of specialization ...</p>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="small-12 large-6 columns">
                    <div class="wow fadeInUp">
                        <div class="feature-3">
                            <div class="feature-3-img-div">
                                <img class="feature-3-img" src="<?php echo base_url('assets/img/1438912418_Permalink.png') ?>" />
                            </div>
                            <div class="feature-3-text">
                                <div class="feature-3-title">
                                    <span>SEO Services</span>
                                </div>
                                <p class="feature-3-text-line">Our wide expertise in the SEO Services has garnered us respected clients in various sectors, industries and areas of specialization ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT SECTION 2 -->
    <div class="section-three" id="portfolio">
        <div class="row">
            <div class="large-12 columns">
                <div class="portfolio">
                    <span>Our Portfolio</span>
                </div>   
            </div>
        </div>
        <div class="row pad-top">
            <div class="wow fadeInUp">
                <ul class="medium-block-grid-5 small-block-grid-2">
                    <li><a class="th" href="#" data-reveal-id="port_modal1"><img src="<?php echo base_url('assets/img/img1.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal2"><img src="<?php echo base_url('assets/img/img2.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal3"><img src="<?php echo base_url('assets/img/img3.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal4"><img src="<?php echo base_url('assets/img/img4.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal5"><img src="<?php echo base_url('assets/img/img5.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal6"><img src="<?php echo base_url('assets/img/img6.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal7"><img src="<?php echo base_url('assets/img/img7.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal8"><img src="<?php echo base_url('assets/img/img8.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal9"><img src="<?php echo base_url('assets/img/img9.jpg'); ?>" /></a></li>
                    <li><a class="th" href="#" data-reveal-id="port_modal10"><img src="<?php echo base_url('assets/img/img11.jpg'); ?>" /></a></li>
                </ul>
            </div>
        </div>
        <div id="port_modal1" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal2" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal3" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal4" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal5" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal6" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal7" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal8" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal9" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
        <div id="port_modal10" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">            
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
    </div> <!-- END CONTENT SECTION 3 -->

    <div class="section-five process">
        <div class="row">
            <div class="large-12 columns">
                <div class="services">
                    <span>Our Process</span>
                </div>   
            </div>
        </div>
        <div class="row">
            <div class="large-4 medium-6 small-12 columns">
                <div class="wow fadeInUp">
                    <div class="process-1">
                        <div class="process-1-img-div">
                            <a href="#"><i class="fa fa-search fa-2x icon-white"></i></a>  
                        </div>
                        <div class="process-1-text">
                            <div class="process-1-title">
                                <span>Assess Needs</span>
                            </div>
                            <p class="process-1-text-line">Requirement gathering and analysis is the first stage of any SDLC model. This phase is basically the brainstorming phase and often consists of sub-stages like Feasibility Analysis to check how much of the idea can be put into action....</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="large-4 medium-6 small-12 columns">
                <div class="wow fadeInUp">
                    <div class="process-2">
                        <div class="process-2-img-div">
                            <a href="#"><i class="fa fa-bolt fa-2x icon-white"></i></a>    
                        </div>
                        <div class="process-2-text">
                            <div class="process-2-title">
                                <span>Design Specifications</span>
                            </div>
                            <p class="process-2-text-line">Includes translation of the requirements specified in the SRS into a logical structure that can be implemented in a programming language. The output of the design phase is a design document that acts as an input for all the subsequent SDLC phases.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="large-4 medium-6 small-12 columns">
                <div class="wow fadeInUp">
                    <div class="process-3">
                        <div class="process-3-img-div">
                            <a href="#"><i class="fa fa-gear fa-2x icon-white"></i></a>
                        </div>
                        <div class="process-3-text">
                            <div class="process-3-title">
                                <span>Development / Testing</span>
                            </div>
                            <p class="process-3-text-line">Includes implementation of the design specified in the design document into executable programming language code and detection of errors in the software. The testing process starts with a test plan that recognizes test-related activities.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-4 medium-6 small-12 columns">
                <div class="wow fadeInUp">
                    <div class="process-4">
                        <div class="process-4-img-div">
                            <a href="#"><i class="fa fa-laptop fa-2x icon-white"></i></a> 
                        </div>
                        <div class="process-4-text">
                            <div class="process-4-title">
                                <span>Implement System</span>
                            </div>
                            <p class="process-4-text-line">This is an important stage of software development life cycle. In this stage, if the software is run on various systems by users. If it runs smoothly on these systems without any flaw, then it is considered ready to be launched.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="large-4 medium-6 small-12 columns">
                <div class="wow fadeInUp">
                    <div class="process-5">
                        <div class="process-5-img-div">
                            <a href="#"><i class="fa fa-gavel fa-2x icon-white"></i></a>
                        </div>
                        <div class="process-5-text">
                            <div class="process-5-title">
                                <span>Support Operations</span>
                            </div>
                            <p class="process-5-text-line">Includes implementation of changes that software might undergo over a period of time, or implementation of new requirements after the software is deployed at the customer location.</p>
                        </div>    
                    </div>
                </div>
            </div>
            <div class="large-4 medium-6 small-12 columns">
                <div class="wow fadeInUp">
                    <div class="process-6">
                        <div class="process-6-img-div">
                            <a href="#"><i class="fa fa-group fa-2x icon-white"></i></a>
                        </div>
                        <div class="process-6-text">
                            <div class="process-6-title">
                                <span>Evaluate Performance</span>
                            </div>
                            <p class="process-6-text-line">The final phase of the SDLC is to measure the effectiveness of the application and evaluate potential enhancements and compare results under the analysis criteria.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="skills-section">
    <!--        <div class="row">
                <div class="large-12 columns">
                    <div class="skills">
                        <span>Our Expertise</span>
                    </div> 
                </div>
            </div>-->
    <div class="row">
        <div class="large-5 small-12 columns">
            <img class="skill-img" src="<?php echo base_url('assets/img/skill.gif'); ?>"
        </div>
    </div>
    <div class="large-6 small-12 columns large-offset-1">
        <div class="progress-bars">
            <div class="row">
                <div class=" small-12 columns">
                    <div class="language">
                        <span>Design</span>
                    </div>
                    <div class="progress large-10">
                        <span style="width: 75%;" class="meter html"><p class="text-center">75%</p></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <div class="language">
                        <span>Develop</span>
                    </div>
                    <div class="progress large-10">
                        <span style="width: 80%;" class="meter css"><p class="text-center">80%</p></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <div class="language">
                        <span>Coding</span>
                    </div>
                    <div class="progress large-10">
                        <span style="width: 95%;" class="meter php"><p class="text-center">95%</p></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <div class="language">
                        <span>Testing</span>
                    </div>
                    <div class="progress large-10">
                        <span style="width: 80%;" class="meter dot-net"><p class="text-center">80%</p></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <div class="language">
                        <span>Deployment</span>
                    </div>
                    <div class="progress large-10">
                        <span style="width: 87%;" class="meter dep"><p class="text-center">87%</p></span>
                    </div>
                </div>
            </div>
            <!--            <div class="row">
                            <div class="large-12 columns">
                                <button class="click" type="submit"><span class="blinker">Click here to show our Skills</span></button>
                            </div>
                        </div>  -->
        </div>
    </div>           
</div>
</div>
<div class="section-four" id="About">
    <div class="row">
        <div class="small-12 large-12 columns">
            <div class="About">
                <span>About US</span>
                <p class="helpPara">IBS offers value to its customers, and provides cost effective Quality Solutions to their problems. We strongly believe in customers satisfaction and potential growth of the society.</p>
            </div>   
        </div>
    </div>
    <div class="row">
        <div class="wow bounceIn">
            <div class="medium-4 columns">
                <p class="para_img"><i class="fa fa-users fa-5x about-icon"></i></p>
                <!--<p class="para_img"><img src="<?php echo base_url('assets/img/round1_03.png'); ?>"></p>-->
                <p class="para_text">We have a team of capable and dedicated IT people </p>
            </div>
            <div class="medium-4 columns">
                <p class="para_img"><i class="fa fa-briefcase fa-5x about-icon"></i></p>
                <!--<p class="para_img"><img src="<?php echo base_url('assets/img/round2_03.png'); ?>"></p>-->
                <p class="para_text">We strongly believe in customers satisfaction</p>
            </div>
            <div class="medium-4 columns">
                <p class="para_img"><i class="fa fa-globe fa-5x about-icon"></i></p>
                <!--<p class="para_img"><img src="<?php echo base_url('assets/img/round3_03.png'); ?>"></p>-->
                <p class="para_text">We strive to build productive Things</p>                                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <p class="para_img_laptop"><img src="<?php echo base_url('assets/img/laptop (1).gif'); ?>"></p> 
        </div>
    </div>
</div>
<!-- END CONTENT SECTION 4 -->
<div class="technologies">
    <div class="row">
        <div class="large-12 columns">
            <div class="tech-head">
                <span>Technologies we are working in</span>
            </div>
        </div>
    </div>
    <div class="marg-top">
        <div class="row">
            <div class="large-3 small-6 columns">
                <img class="tech-img" src="<?php echo base_url('assets/img/tech/foundation.gif'); ?>"/>
            </div>
            <div class="large-3 small-6 columns">
                <img class="tech-img" src="<?php echo base_url('assets/img/tech/php.gif'); ?>"/>
            </div>
            <div class="large-3 small-6 columns">
                <img class="tech-img" src="<?php echo base_url('assets/img/tech/codeigniter.gif'); ?>"/>
            </div>
            <div class="large-3 small-6 columns">
                <img class="tech-img" src="<?php echo base_url('assets/img/tech/kohana.gif'); ?>"/>
            </div>
            <div class="large-3 small-6 columns">
                <img class="tech-img" src="<?php echo base_url('assets/img/tech/dot-net.gif'); ?>"/>
            </div>
            <div class="large-3 small-6 columns">
                <img class="tech-img wp-img" src="<?php echo base_url('assets/img/tech/wordpress.jpg'); ?>"/>
            </div>
            <div class="large-3 small-6 columns">
                <img class="tech-img" src="<?php echo base_url('assets/img/tech/fb-app.gif'); ?>"/>
            </div>
            <div class="large-3 small-6 columns">
                <img class="tech-img magento-img" src="<?php echo base_url('assets/img/tech/magento.gif'); ?>"/>
            </div>
        </div>
    </div>
</div>
<div class="section-six" id="Contact">
    <div class="row">
        <div class="small-12 large-12 columns">
            <div class="contact">
                <span>Contact US</span>                    
            </div>   
        </div>
    </div>
    <div class="row">
        <div class="large-6 columns">
            <div class="feilds">
                <div class="textfeild">
                    <form action="<?php echo base_url('Insignia/email_test'); ?>" method="post" accept-charset="utf-8">
                        <!--<div class="nameIcon"></div>-->
                        <input type="text" name="name" placeholder="Full Name" value="">                     

                        <!--<div class="emailIcon"></div>-->
                        <input type="email" name="email" placeholder="Email" value="">    

                        <div class="message"><span class="messageIcon"></span>
                            <textarea class="text-msg" name="message" rows="3" cols="67" placeholder="Message"></textarea>
                        </div>
                        <div class="submit"><input type="submit" id="submit" name="submit" value="Submit"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="contactInfo">

                <div class="contact-location-img"><i class="fa fa-map-marker fa-2x cont-icon"></i></div>
                <p class="contact-text"> Office # 216-A Saddique Trade Center, Gulberg Lahore.</p>


                <div class="contact-mobile-img"><i class="fa fa-phone fa-2x cont-icon"></i></div>
                <p class="contact-text">03214192343</p>


                <div class="contact-email-img"><i class="fa fa-envelope fa-2x cont-icon"></i></div>
                <p class="contact-text"> info@insigniabiz.com </p>

            </div>
        </div>
    </div>
</div>
<!--FOOTER SECTION--> 
<div class="footer-section">
    <div class="row">
        <div class="large-4 columns">
            <div class="links">
                <!--<a href="https://www.facebook.com/pages/Insignia-Business-Solutions/800754183318301" target="_blank"><img src="https://localhost/insignia/insigniabiz/assets/img/s1_03.png"></a> <a href="#"><img src="https://localhost/insignia/insigniabiz/assets/img/s2_03.png"></a> <a href="#"><img src="https://localhost/insignia/insigniabiz/assets/img/s3_03.png"></a> <a href="#"><img src="https://localhost/insignia/insigniabiz/assets/img/s4_03.png"></a> <a href="#"><img src="https://localhost/insignia/insigniabiz/assets/img/s5_03.png"></a>-->
                <a href="https://www.facebook.com/pages/Insignia-Business-Solutions/800754183318301" target="_blank"><i class="fa fa-facebook-square fa-2x social-icon"></i></a> <a href="#"><i class="fa fa-twitter-square fa-2x social-icon"></i></a> <a href="#"><i class="fa fa-linkedin-square fa-2x social-icon"></i></a> <a href="#"><i class="fa fa-rss-square fa-2x social-icon"></i></a> <a href="#"><i class="fa fa-google-plus-square fa-2x social-icon"></i></a>
            </div>
        </div>


        <div class="large-4 columns">
            <div class="move-up">
                <a href="#home" target="_self">
                    <i  class="fa fa-arrow-circle-up fa-2x up-icon"></i>
                </a>
            </div>
        </div>
        <div class="large-4 columns">
            <div class="footer-text">
                <p>&copy; 2015 Insignia. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</div> 
<!-- END FOOTER SECTION -->

<!-- JAVASCRIPTS -->

<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>    
<script src="<?php echo base_url('assets/js/lightbox.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/foundation.min.js'); ?>"></script>
<!--<script>//window.jQuery || document.write('<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js'); ?>"><\/script>')</script>--> 
<script src="<?php echo base_url('assets/js/wow.min.js'); ?>"></script>
<script>     new WOW().init();
</script>
<script src="assets/js/app.js"></script>
<script src="assets/js/init.js"></script>
<script>
//    $(".click").click(function () {
//        $(".html").append('75%').addClass( "complete" ).animate({width: "75%"},3000);
//        $(".css").append('80%').animate({width: "80%"},2000);
//        $(".php").append('95%').animate({width: "95%"},1000);
//        $(".dot-net").append('80%').animate({width: "80%"},2000);
//        $(".dep").append('87%').animate({width: "87%"},3000);
//    });

	$(function () {

		var $meters = $(".progress > span");
		var $section = $('.progress-bars');
		var $queue = $({});

		function loadDaBars() {
			$meters.each(function () {
				var $el = $(this);
				var origWidth = $el.width();
				$el.width(0);
				$queue.queue(function (next) {
					$el.animate({width: origWidth}, 500, next);
				});
			});
		}

		$(document).bind('scroll', function (ev) {
			var scrollOffset = $(document).scrollTop();
			var containerOffset = $section.offset().top - window.innerHeight;
			if (scrollOffset > containerOffset) {
				loadDaBars();
				// unbind event not to load scrolsl again
				$(document).unbind('scroll');
			}
		});
	});


</script>
<script>

	$(function () {
// Slow slides for internal links
		$('a[href*=#]:not([href=#])').click(function () {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - 30
					}, 1500);
					if ($(window).width() < 600)
					{
						$('nav ul').slideUp();
					}
					return false;
				}
			}
		});

		$("a[href='#home']").click(function () {
			$("html, body").animate({scrollTop: 0}, "slow");
			return false;
		});
	});

</script>

<script>
	$(document).ready(function () {
		$('#login').click(function () {
			event.preventDefault();
			var mail = $('#email').val();
			var pass = $('#password').val();
			//                    if (mail == '') {
			////                        $('p.error').html('Enter email')
			////                        $('p.error').slideDown();
			//                        alert('Enter email');
			//                    } else {
			////                        $('p.error').html('Please enter Correct Email')
			////                        $('p.error').slideDown();
			//                        alert('Please enter Correct Email');
			//                    }
			//                    if (pass == '') {
			////                         $('p.error').html('Enter Password')
			////                        $('p.error').slideDown()
			//                        alert('Enter Password')
			//                    } else {
			//                        $('p.error').html('Please enter Correct Password')
			//                        $('p.error').slideDown()
			if ((mail == '') && (pass == '')) {
				$('p.error').html('Enter Email or Password')
				$('p.error').slideDown()
				//                        alert('Please enter Correct Password');
			}
			else {
				$('p.error').html('Wrong Email or Password')
				$('p.error').slideDown('slow')
			}
		});

	});
</script>

</body>
</html>
