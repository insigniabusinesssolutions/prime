<?php

class Participants_model extends CI_Model {

    public function __construct() {
        
    }

    function save($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function get_participants($id = null) {
        if ($id)
            return $this->find('participants', '*', array('participants.id' => $id, 'participants.is_deleted' => 0));
        else
            return $this->find('participants', '*', array('is_deleted' => 0));
    }

    public function get_participant_by_image($image_id) {
        return $this->find('participants', '*', array('participants.image_id' => $image_id, 'is_deleted' => 0));
    }

    function find($table, $select = false, $where = false, $joins = false, $joins_on = false, $group = false, $order = false, $having = false, $limit1 = false, $limit2 = false, $where_in = FALSE, $where_in_col = FALSE) {
        if ($select) {
            $this->db->select($select);
        } else {
            $this->db->select('*');
        }

        if (is_array($joins) && is_array($joins_on)) {
            $index = 0;
            foreach ($joins as $join) {
                $this->db->join($join, $joins_on[$index]);
                $index++;
            }
        } else {
            if ($joins && $joins_on) {
                $this->db->join($joins, $joins_on);
            }
        }

        if ($where) {
            //$where array or string
            $this->db->where($where);
        }
        if ($where_in) {
            //$where array or string
            $this->db->where_in($where_in_col, $where_in);
        }
        if ($group) {
            //$group array or string
            $this->db->group_by($group);
        }
        if ($order) {
            //$order string
            $this->db->order_by($order, 'desc');
        }
        if ($having) {
            //$having string or array
            $this->db->having($having);
        }
        if (!$limit2 && $limit1) {
            $this->db->limit($limit1);
        } else {
            if ($limit1) {
                $this->db->limit($limit1, $limit2);
            }
        }
        $query = $this->db->get($table);
        //echo $this->db->last_query(); die;
        //echo mysql_error();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function update_coins($fb_id, $stamps, $coins) {

        $round_set = " game_1_rounds = game_1_rounds + 1 ";

        $update = "UPDATE participants SET coins = coins +'$coins' , stamps = stamps+'$stamps' , $round_set  where facebook_id = '$fb_id'";
        return $this->db->query($update);
    }

    //update
    function update($data, $where) {
        $this->db->update('participants', $data, $where);
        return $this->db->affected_rows();
    }

//    delet
    function delete($id = null) {
        $this->db->update('participants', array('is_deleted' => 1), array('id' => $id));
        return $this->db->affected_rows();
    }

    function delete_by_imageid($image_id = null) {
        $this->db->update('participants', array('is_deleted' => 1), array('image_id' => $image_id));
        return $this->db->affected_rows();
    }

}
